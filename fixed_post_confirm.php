<?php 
	public function post_confirm() {

		$servicio = Service::find(Input::get('service_id'));
		$error = array('error' => '0');

		if ($servicio != NULL) {

			if ($servicio->driver_id == NULL && $servicio->status_id == '1') {
				$driverTmp = Driver::find(Input::get('driver_id'));
				Service::update($servicio->id, 
								array('driver_id' => $driverTmp->id,
									  'car_id' => $driverTmp->car_id,
									  'status_id' => '2'));

				Driver::update($driverTmp->id, array("available" => '0'));

				//Notificar a usuario!!
				if ($servicio->user->uuid != '') {
					$push = Push::make();

					if ($servicio->user->type == '1'){ //iPhone
						$device = 'ios';
						$sound = 'honk.wav';
					}else{
						$device = 'android2';
						$sound = 'default';
					}

					$result = $push->$device($servicio->user->uuid, 'Tu servicio ha sido confirmado!', 1, $sound, 'Open', array('serviceId' => $servicio->id));
				}

			}else{

				$error['error'] = '1';
				if ($servicio->status_id == '6') {
					$error['error'] = '2';
				}
				
			}

		}else{
			$error['error'] = '3';
		}
		return Response::json($error);
	}
?>